
use AutoVerleihDB

create proc printPersonInfo
--alter proc printPersonInfo
 @p_id int
as
begin
	print 'Folgende Person wurde in die Tabelle T_Persom hinzugefügt:' 
	select * from T_Person
	where  p_id = @p_id
end;
--exec printPersonInfo 2
--insert into T_Person values ('B072EEE2133', 'Frau', 'Mona', 'Klari', '15.07.1988', GETDATE(), 'Bernd 5', 1);
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
create trigger CheckInsertPersonTrigger
--alter trigger CheckInsertPersonTrigger
on T_Person
for insert
as
begin
declare @p_id int
set @p_id = (select p_id from inserted)
print 'Es folgt Personen Info' 
exec printPersonInfo @p_id
end;
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++