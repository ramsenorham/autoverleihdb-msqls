use AutoVerleihDB;

create function get_basis_preis
(
-- suche nach dem vorhandenen basis_preis des Autos
	@marke nvarchar(30),
	@modell nvarchar(30)
)
returns money -- die Funktion liefert basis_preis als Ergebnis zur�ck
as
begin
	-- Deklaration des Variabl
	declare @basis_preis money

		select @basis_preis = mmp.mm_preis from R_Marke_Modell_Preis mmp
	
	where 
		mmp.ma_id in (select ama.ma_id from T_Auto_Marke ama where ama.marke=@marke) and
		mmp.mo_id in (select amo.mo_id from T_Auto_Modell amo where amo.modell=@modell)
if (@basis_preis is null)
set @basis_preis = -1
return @basis_preis
end;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


