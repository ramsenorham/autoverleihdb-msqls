use AutoVerleihDB;

--alter function get_dauer(@von datetime, @bis datetime) 
create function get_dauer(@von datetime, @bis datetime) 
-- mit dauer, finden wir 
--interne Regeln 1day Basis ----10 days basis * 10 * Rabat 
returns int
as
begin
declare @dauer int
set @dauer = (select DATEDIFF (DAY , @von, @bis ))
return @dauer
end;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++