Use AutoVerleihDB;

--alter proc insertNeuePerson
create proc insertNeuePerson
-- insert into T_Person und T_Firma
@fuehrerschein char(11),
@titel char(10),
@vorname nvarchar(50),
@nachname nvarchar(50),
@geboren datetime,
@strasseNr nvarchar(50),
@plz char(5),
@ort nvarchar(30)
as
begin
declare @p_id int
declare @o_id int
if not exists (select o.o_id from T_Ort o where o.plz = @plz and o.ort = @ort)
begin
insert into T_Ort values(@plz, @ort)
print'erfolgreich neue Ort hinzugefügt.'
set @o_id = (select max(o_id) from T_ort)
end
else 
begin
print'Ort ist schon existiert'
set @o_id = (select o.o_id from T_ort o where o.plz = @plz and o.ort = @ort)
end

if ( (select dbo.get_p_id('@fuehrerschein')) = -1)
begin
insert into T_Person values(@fuehrerschein, @titel, @vorname, @nachname, @geboren, CONVERT (date, GETDATE()))

print'erfolgreich neue Person hinzugefügt.'
set @p_id = (select max(p_id) from T_Person)
insert into R_PersonAdresse values(@p_id, @o_id, @strasseNr, '1')
end
else
begin
print'Person ist schon existiert'
end

end;
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++