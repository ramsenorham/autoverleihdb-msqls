use AutoVerleihDB;

create proc insertNeueMietvertrag
--alter proc insertNeueMietvertrag

@fuehrerschein char(11),
@marke nvarchar(30),
@modell nvarchar(30),
@von datetime,
@bis datetime,
@service nvarchar (200)
as
begin

declare @mm_id int
set @mm_id = (select dbo.get_mm_id(@marke, @modell))

declare @p_id int
set @p_id = (select dbo.get_p_id(@fuehrerschein))

declare @dauer int 
set @dauer = (select dbo.get_dauer(@von, @bis))

declare @basis_preis money
set @basis_preis = (select dbo.get_basis_preis(@marke , @modell))

declare @preis_days money
set @preis_days = (@basis_preis * @dauer)

set xact_abort on
begin transaction
--Haupt Insert
insert into R_Mietvertrag (p_id, mm_id, gesamt_preis, vertrag_datum, von, bis) values (@p_id,@mm_id, 0, CONVERT (date, GETDATE()), @von, @bis) 

declare @mv_id int
set @mv_id = (select max(mv_id) from R_Mietvertrag) 

declare @list varchar(200)
declare @pos int
declare @len int
declare @value varchar (200)

set @list = @service 
set @pos=0
set @len=0

while CHARINDEX (',', @list, @pos) >0
   begin 
	set @len = CHARINDEX (',', @list, @pos+1) - @pos
	set @value = SUBSTRING(@list, @pos,@len)
			insert into R_Service (mv_id, zs_id) 
			values (@mv_id, (select zs_id from T_Zusatzservice where bezeichnung = @value))
		print'Service hinzugefügt'
		set @pos = CHARINDEX (',', @list, @pos+@len) +1
   end
declare @zusatzPreis money
set @zusatzPreis = (select sum (zs.zs_preis) from R_Service s inner join T_Zusatzservice zs on s.zs_id = zs.zs_id where s.mv_id = @mv_id)

update R_Mietvertrag set gesamt_preis = @preis_days + @zusatzPreis where mv_id = @mv_id
commit
end;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++