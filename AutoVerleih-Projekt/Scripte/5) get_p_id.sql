use AutoVerleihDB;

create function get_p_id
(
-- suche nach dem neu oder vorhandenen Person
	@fuehrerschein char(11)
)
returns int -- die Funktion liefert p_id als Ergebnis zur�ck
as
begin
-- Deklaration des Variabl 
declare @p_id int
select @p_id = p_id from T_person where fuehrerschein = @fuehrerschein
if (@p_id is null)
set @p_id = -1
return @p_id
end;

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--select dbo.get_p_id('B072RRE2152');