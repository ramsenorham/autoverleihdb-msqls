
use master;
-- Datenbank anlegen: 'CREATE DATABASE'
create database AutoVerleihDB;
--*************************************************
use AutoVerleihDB;
--**************************************************
--drop table R_FirmaAdresse;
--drop table R_PersonAdresse;
--drop table T_Person;
--drop table T_Firma;
--drop table T_Ort;
--drop table R_Firma_Mietvertrag;
--drop table R_Mietvertrag;
--drop table T_Auto_Marke;
--drop table T_Auto_Modell;
--drop table R_Marke_Modell_Preis;
--drop table T_Zusatzservice;
--drop table R_Service;
--**************************************************
create table T_Ort
(
    o_id int not null identity,
    plz char(5) not null,           
    ort nvarchar(30) not null
    -- Primärschschlüssel für die Spalte plz
    constraint pkOrt primary key(o_id)
);
 --*************************************************
create table T_Firma
(
    f_id int not null identity,
    Firmenname nvarchar(50) not null 
    -- Primärschschlüssel für die Spalte fi_id
    constraint pkFirma primary key(f_id)
);
--*************************************************
create table T_Person
(
    p_id int not null identity, 
    fuehrerschein char(11) unique not null,
    titel        char(10)    null,
    vorname nvarchar(50)    null,
    nachname nvarchar(50)    not null,
    geboren datetime null,
    eintritt datetime  not null
    -- Primärschschlüssel für die Spalte p_id
    constraint pkPerson primary key(p_id)
);
--*************************************************
create table R_PersonAdresse
(
    p_id int not null,
    o_id int not null,
    strasseNr nvarchar(50) not null,
	aktuelle_Adresse char(1) not null
    -- Primärschschlüssel für die Spalte p_id, o_id
    constraint pkPersonAdresse primary key(p_id, o_id)
);
--******************************************************
create table R_FirmaAdresse
(
	f_id int not NULL,	
	o_id int not NULL,
	strasseNr nvarchar(50) not null,
	aktuelle_Adresse char(1) not null
-- Primärschschlüssel für die Spalten zs_id, mv_id
	constraint pkR_FirmaAdresse primary key(f_id, o_id)
);
--*************************************************
create table T_Auto_Marke  
(
    ma_id int not NULL identity,
    marke nvarchar(30) not null
    -- Primärschschlüssel für die Spalte ma_id
    constraint pkAuto_Marke primary key(ma_id)
);
--*************************************************
create table T_Auto_Modell  
(
    mo_id int not NULL identity,
    modell nvarchar(30) not null
    -- Primärschschlüssel für die Spalte mo_id
    constraint pkAutoModell primary key(mo_id)
);
   --###############################################################
create table R_Marke_Modell_Preis  
(
    mm_id int not NULL identity,
    ma_id int not NULL, 
    mo_id int not NULL,
	kennzeichen nvarchar(9) unique not null,
	mm_preis money not NULL     
    -- Primärschschlüssel für die Spalte mm_id
    constraint pkMarkeModellPreis primary key(mm_id)
);
--************************************************
create table T_Zusatzservice
(
    zs_id int not NULL identity,
    bezeichnung nvarchar(50) not NULL,
	zs_preis money not null 
    -- Primärschschlüssel für die Spalte zs_id
    constraint pkZusatzservice primary key(zs_id)
);
--********************************************************************
create table R_Service
(
	mv_id int not NULL,	
	zs_id int not NULL
-- Primärschschlüssel für die Spalten zs_id, mv_id
	constraint pkService primary key(mv_id, zs_id)
);
--**********************************************************
create table R_Mietvertrag
(
    mv_id int    not null identity,
	p_id int not null,
    mm_id int not NULL, 
    gesamt_preis money,
    vertrag_datum datetime not null,
    von datetime not null ,
    bis datetime not null 
    -- Primärschschlüssel für die Spalte mi_id
    constraint pkMietvertrag primary key(mv_id)
);
--************************************************************************************
create table R_Firma_Mietvertrag
(
    mv_id int not null,
    f_id int not null 
    -- Primärschschlüssel für die Spalte fm_id
    constraint pkFirmaMietvertrag primary key(mv_id,f_id)
);
--############################## Index #################################
CREATE INDEX IX_Ort  ON T_Ort (plz);
CREATE INDEX IX_Person ON T_Person (fuehrerschein);
--############################## constraint #################################
--R_FirmaAdresse Fremdschschlüssel für die Spalten o_id, f_id 
alter table R_FirmaAdresse add constraint fkFirmaAdresse_Ort foreign key(o_id) references T_Ort(o_id);
alter table R_FirmaAdresse add constraint fkFirmaAdresse_Firma foreign key(f_id) references T_firma(f_id);
--R_PersonAdresse Fremdschschlüssel für die Spalten o_id, p_id 
alter table R_PersonAdresse add constraint fkPersonAdresse_Ort foreign key(o_id) references T_Ort(o_id);
alter table R_PersonAdresse add constraint fkPersonAdresse_Person foreign key(p_id) references T_Person(p_id);
--R_Service Fremdschschlüssel für die Spalten zs_id, mv_id
alter table R_Service add constraint fkService_Zusatzservice foreign key(zs_id) references T_Zusatzservice(zs_id);
alter table R_Service add constraint fkService_Mietvertrag foreign key(mv_id) references R_Mietvertrag(mv_id);
--R_Marke_Modell_Preis Fremdschschlüssel für die Spalten ma_id, mo_id
alter table R_Marke_Modell_Preis add constraint fkMarkeModellPreis_Auto_Marke foreign key(ma_id) references T_Auto_Marke(ma_id);
alter table R_Marke_Modell_Preis add constraint fkMarkeModellPreis_Auto_Modell foreign key(mo_id) references T_Auto_Modell(mo_id);
--R_Mietvertrag Fremdschschlüssel für die Spalten p_id, mm_id
alter table R_Mietvertrag add constraint fkPersonMietvertrag foreign key(p_id) references T_Person(p_id);
alter table R_Mietvertrag add constraint fkAutoPreis foreign key(mm_id) references R_Marke_Modell_Preis(mm_id);
--R_Firma_Mietvertrag Fremdschschlüssel für die Spalten f_id und mv_id
alter table R_Firma_Mietvertrag add constraint fkFirma_MietvertragMietvertrag foreign key(mv_id) references R_Mietvertrag(mv_id);
alter table R_Firma_Mietvertrag add constraint fkFirma_MietvertragFirma foreign key(f_id) references T_Firma(f_id); 
--############################### Inserts ################################
insert into T_Ort values ('30161', 'Hannover');
insert into T_Ort values ('52062', 'Kreisfreie Stadt');
insert into T_Ort values ('30161', 'Hannover');
insert into T_Ort values ('78267', 'Freiburg');
insert into T_Ort values ('54298', 'Trier-Saarburg');
insert into T_Ort values ('93326', 'Kelheim');
insert into T_Ort values ('97355', 'Kitzingen');
insert into T_Ort values ('30167', 'Hannover');
insert into T_Ort values ('30159', 'Hannover');
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
insert into T_Firma (Firmenname) values ('Sparkasse');
insert into T_Firma values ('K&L Ruppert');
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
insert into R_FirmaAdresse (F_id, o_id, strasseNr, aktuelle_Adresse) values (1,3,'RenateStr. 5', 1);
insert into R_FirmaAdresse (F_id, o_id, strasseNr, aktuelle_Adresse) values (2,2,'RuppertStr. 6', 1);
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
insert into T_Person values ('B072RRE2152', 'Herr', 'Martin', 'Merkel', '21.07.1955', '16.05.2018');
insert into T_Person values ('B074ZZE2142', 'Frau', 'Sabine', 'Smidt', '03.08.1975', '16.06.2018');
insert into T_Person values ('B072MME2144', 'Frau', 'Elke', 'Pilmaier', '15.02.1968', '16.07.2018');
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
insert into R_PersonAdresse (p_id, o_id, strasseNr, aktuelle_Adresse) values (1,2,'FriedenStr. 5', '1');
insert into R_PersonAdresse (p_id, o_id, strasseNr, aktuelle_Adresse) values (2,4,'WilhelmStr. 66','1');
insert into R_PersonAdresse (p_id, o_id, strasseNr, aktuelle_Adresse) values (3,1,'WarStr. 12','1');
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
insert into T_Auto_Marke (marke) values ('BMW') ;
insert into T_Auto_Marke (marke) values ('Benz') ;
insert into T_Auto_Marke (marke) values ('Ford') ;
insert into T_Auto_Marke (marke) values ('VW') ;
insert into T_Auto_Marke (marke) values ('Opel') ;
insert into T_Auto_Marke (marke) values ('Audi') ; 
insert into T_Auto_Marke (marke) values ('Toyota')  ;
insert into T_Auto_Marke (marke) values ('Skoda') ; 
insert into T_Auto_Marke (marke) values ('Volvo')  ;
insert into T_Auto_Marke (marke) values ('Porsche')  ;
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
insert into T_Auto_Modell (modell) values ('BMW 2er Gran Tourer');
insert into T_Auto_Modell (modell) values ('BMW 1er');
insert into T_Auto_Modell (modell) values ('BMW 2er');
insert into T_Auto_Modell (modell) values ('BMW 4er');
insert into T_Auto_Modell (modell) values ('BMW i3');
insert into T_Auto_Modell (modell) values ('BMW X3');
insert into T_Auto_Modell (modell) values ('BMW X5');
insert into T_Auto_Modell (modell) values ('BMW X5');
insert into T_Auto_Modell (modell) values ('BMW X6');
--VW
insert into T_Auto_Modell (modell) values ('VW Amarok');
insert into T_Auto_Modell (modell) values ('VW Arteon');
insert into T_Auto_Modell (modell) values ('VW Beetle');
insert into T_Auto_Modell (modell) values ('VW Caddy');
insert into T_Auto_Modell (modell) values ('VW Crafter');
insert into T_Auto_Modell (modell) values ('VW Golf');
insert into T_Auto_Modell (modell) values ('VW Polo');
insert into T_Auto_Modell (modell) values ('VW T-Cross');
insert into T_Auto_Modell (modell) values ('VW CrossPolo');
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
insert into R_Marke_Modell_Preis values (1, 1, 'HH558z4', 23);
insert into R_Marke_Modell_Preis values (1, 2, 'HK98HM8', 24);
insert into R_Marke_Modell_Preis values (1, 3, 'HH33HO', 26);
insert into R_Marke_Modell_Preis values (1, 4, 'MM678F4', 30);
insert into R_Marke_Modell_Preis values (1, 5, 'SE552K', 15);
insert into R_Marke_Modell_Preis values (1, 6, 'JK785G', 43);
insert into R_Marke_Modell_Preis values (1, 7, 'OP873G', 22);
insert into R_Marke_Modell_Preis values (1, 8, 'HQ887H', 44);
insert into R_Marke_Modell_Preis values (1, 9, 'D99KJ', 54);
insert into R_Marke_Modell_Preis values (4, 10, 'HL9945', 27);
insert into R_Marke_Modell_Preis values (4, 11, 'UU6864R', 26);
insert into R_Marke_Modell_Preis values (4, 12, 'GH6839H', 20);
insert into R_Marke_Modell_Preis values (4, 13, 'WW863G', 33);
insert into R_Marke_Modell_Preis values (4, 14, 'AA96732J', 28);
insert into R_Marke_Modell_Preis values (4, 15, 'JJD775t', 23);
insert into R_Marke_Modell_Preis values (4, 16, 'OI5534H', 24);
insert into R_Marke_Modell_Preis values (4, 17, 'UZ8864J', 26);
insert into R_Marke_Modell_Preis values (4, 18, 'WE538K', 30);
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
insert into T_Zusatzservice (bezeichnung, zs_preis) values ('Navigation', 5);
insert into T_Zusatzservice (bezeichnung, zs_preis) values ('Vollkasko ohne Selbstbeteiligung', 15);
insert into T_Zusatzservice (bezeichnung, zs_preis) values ('Schneeketten', 25);
insert into T_Zusatzservice (bezeichnung, zs_preis) values ('Reifenversicherung', 60);
--++++++++++++++++++++++++++++++++sonstiges++++++++++++++++++++++++++++++++++++++++++++++++++++++
--alter table R_FirmaAdresse add aktuelle_Adresse char(1) not null default '1'
--drop index IX_Ort ON T_Ort;
--DROP INDEX T_Person.IX_Person;
--CREATE NONCLUSTERED INDEX IDX_Person ON T_Person(fuehrerschein);
--alter table T_Firma drop constraint fkFirmaAdresse_Firma;
--alter table T_firma drop column o_id



