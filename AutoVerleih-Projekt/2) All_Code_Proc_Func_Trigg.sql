use AutoVerleihDB;

create function get_basis_preis
(
-- suche nach dem vorhandenen basis_preis des Autos
	@marke nvarchar(30),
	@modell nvarchar(30)
)
returns money -- die Funktion liefert basis_preis als Ergebnis zur�ck
as
begin
	-- Deklaration des Variabl
	declare @basis_preis money

		select @basis_preis = mmp.mm_preis from R_Marke_Modell_Preis mmp
	
	where 
		mmp.ma_id in (select ama.ma_id from T_Auto_Marke ama where ama.marke=@marke) and
		mmp.mo_id in (select amo.mo_id from T_Auto_Modell amo where amo.modell=@modell)
if (@basis_preis is null)
set @basis_preis = -1
return @basis_preis
end;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
create function get_mm_id
(
-- suche nach dem vorhandenen Auto
	@marke nvarchar(30),
	@modell nvarchar(30)
)
returns int -- die Funktion liefert mm_id als Ergebnis zur�ck
as
begin
	-- Deklaration des Variabl 
	declare @mm_id int

		select @mm_id = mmp.mm_id from R_Marke_Modell_Preis mmp
	
	where 
		mmp.ma_id in (select ama.ma_id from T_Auto_Marke ama where ama.marke=@marke) and
		mmp.mo_id in (select amo.mo_id from T_Auto_Modell amo where amo.modell=@modell)
if (@mm_id is null)
	set @mm_id = -1
return 	@mm_id
end;
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
create function get_p_id
(
-- suche nach dem neu oder vorhandenen Person
	@fuehrerschein char(11)
)
returns int -- die Funktion liefert p_id als Ergebnis zur�ck
as
begin
-- Deklaration des Variabl 
declare @p_id int
select @p_id = p_id from T_person where fuehrerschein = @fuehrerschein
if (@p_id is null)
set @p_id = -1
return @p_id
end;

--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
create function get_dauer(@von datetime, @bis datetime) 
-- mit dauer, finden wir 
--interne Regeln 1day Basis ----10 days basis * 10 * Rabat 
returns int
as
begin
declare @dauer int
set @dauer = (select DATEDIFF (DAY , @von, @bis ))
return @dauer
end;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
create view V_Auto_basis_preis
as

	select mm_id, mmp.mm_preis As Basis_Preis , ak.marke as Auto_Marke, am.modell as Auto_Modell 
	from R_Marke_Modell_Preis mmp inner join T_Auto_Marke ak on mmp.ma_id =ak.ma_id 
		inner join T_Auto_Modell am on mmp.mo_id=am.mo_id;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
create view V_MietvertragErgebnis as
--alter view V_MietvertragErgebnis as 
select 
mv.p_id, 
p.fuehrerschein, 
p.titel, 
p.vorname + ' ' + p.nachname as [Name], 
p.geboren , 
p.eintritt, 
pa.strasseNr,
o.plz + ' ' + o.ort as Stadt,
mv.gesamt_preis as Summe_Vertrag, 
dbo.get_dauer(mv.von , mv.bis) as Duer_Miete,
ama.Marke +' - '+ amo.Modell as AutoMarke_Modell
from T_Person p inner join R_Mietvertrag mv on p.p_id = mv.p_id inner join R_PersonAdresse pa on p.p_id = pa.p_id inner join T_Ort o on pa.o_id = o.o_id
inner join R_Marke_Modell_Preis mmp on mv.mm_id = mmp.mm_id inner join T_Auto_Marke ama on mmp.ma_id = ama.ma_id inner join T_Auto_Modell amo on mmp.mo_id = amo.mo_id

--select * from V_MietvertragErgebnis
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
create proc printPersonInfo
--alter proc printPersonInfo
 @p_id int
as
begin
	print 'Folgende Person wurde in die Tabelle T_Persom hinzugef�gt:' 
	select * from T_Person
	where  p_id = @p_id
end;
--exec printPersonInfo 2
--insert into T_Person values ('B072EEE2133', 'Frau', 'Mona', 'Klari', '15.07.1988', GETDATE(), 'Bernd 5', 1);
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
create trigger CheckInsertPersonTrigger
--alter trigger CheckInsertPersonTrigger
on T_Person
for insert
as
begin
declare @p_id int
set @p_id = (select p_id from inserted)
print 'Es folgt Personen Info' 
exec printPersonInfo @p_id
end;
--+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--alter proc insertNeuePerson
create proc insertNeuePerson
-- insert into T_Person und T_Firma
@fuehrerschein char(11),
@titel char(10),
@vorname nvarchar(50),
@nachname nvarchar(50),
@geboren datetime,
@strasseNr nvarchar(50),
@plz char(5),
@ort nvarchar(30)
as
begin
declare @p_id int
declare @o_id int
if not exists (select o.o_id from T_Ort o where o.plz = @plz and o.ort = @ort)
begin
insert into T_Ort values(@plz, @ort)
print'erfolgreich neue Ort hinzugef�gt.'
set @o_id = (select max(o_id) from T_ort)
end
else 
begin
print'Ort ist schon existiert'
set @o_id = (select o.o_id from T_ort o where o.plz = @plz and o.ort = @ort)
end

if ( (select dbo.get_p_id('@fuehrerschein')) = -1)
begin
insert into T_Person values(@fuehrerschein, @titel, @vorname, @nachname, @geboren, CONVERT (date, GETDATE()))

print'erfolgreich neue Person hinzugef�gt.'
set @p_id = (select max(p_id) from T_Person)
insert into R_PersonAdresse values(@p_id, @o_id, @strasseNr, '1')
end
else
begin
print'Person ist schon existiert'
end

end;
--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
create proc insertNeueMietvertrag
--alter proc insertNeueMietvertrag

@fuehrerschein char(11),
@marke nvarchar(30),
@modell nvarchar(30),
@von datetime,
@bis datetime,
@service nvarchar (200)
as
begin

declare @mm_id int
set @mm_id = (select dbo.get_mm_id(@marke, @modell))

declare @p_id int
set @p_id = (select dbo.get_p_id(@fuehrerschein))

declare @dauer int 
set @dauer = (select dbo.get_dauer(@von, @bis))

declare @basis_preis money
set @basis_preis = (select dbo.get_basis_preis(@marke , @modell))

declare @preis_days money
set @preis_days = (@basis_preis * @dauer)

set xact_abort on
begin transaction
--Haupt Insert
insert into R_Mietvertrag (p_id, mm_id, gesamt_preis, vertrag_datum, von, bis) values (@p_id,@mm_id, 0, CONVERT (date, GETDATE()), @von, @bis) 

declare @mv_id int
set @mv_id = (select max(mv_id) from R_Mietvertrag) 

declare @list varchar(200)
declare @pos int
declare @len int
declare @value varchar (200)

set @list = @service 
set @pos=0
set @len=0

while CHARINDEX (',', @list, @pos) >0
   begin 
	set @len = CHARINDEX (',', @list, @pos+1) - @pos
	set @value = SUBSTRING(@list, @pos,@len)
			insert into R_Service (mv_id, zs_id) 
			values (@mv_id, (select zs_id from T_Zusatzservice where bezeichnung = @value))
		print'Service hinzugef�gt'
		set @pos = CHARINDEX (',', @list, @pos+@len) +1
   end
declare @zusatzPreis money
set @zusatzPreis = (select sum (zs.zs_preis) from R_Service s inner join T_Zusatzservice zs on s.zs_id = zs.zs_id where s.mv_id = @mv_id)

update R_Mietvertrag set gesamt_preis = @preis_days + @zusatzPreis where mv_id = @mv_id
commit
end;

--++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
create proc updateMietvertrag
--alter proc updateMietvertrag
@mv_id int,
@bis datetime

as
begin

declare @von datetime
declare @mm_id int

select @mm_id = mm_id, @von = von from R_Mietvertrag  where mv_id = @mv_id

declare @dauer int 
set @dauer = (select dbo.get_dauer(@von, @bis) )

declare @basis_preis money
set @basis_preis = (select mm_preis from R_Marke_Modell_Preis where mm_id = @mm_id)

declare @preis_days money
set @preis_days = (@basis_preis * @dauer)

declare @zusatzPreis money
set @zusatzPreis = (select sum (zs.zs_preis) from R_Service s inner join T_Zusatzservice zs on s.zs_id = zs.zs_id where s.mv_id = @mv_id)

--update Mietvertrag
update R_Mietvertrag set bis = @bis , gesamt_preis = @preis_days + @zusatzPreis
where mv_id = @mv_id

end;

--****************************************************************
create proc removeMietvertrag
--alter proc removeMietvertrag
@fuehrerschein char(11),
@vertrag_datum datetime
as
begin

declare @p_id int
set @p_id = (select dbo.get_p_id(@fuehrerschein) )
if (@p_id > 0)
begin 
declare @mv_id int
set @mv_id =(select mv_id from R_Mietvertrag where p_id = @p_id and vertrag_datum = @vertrag_datum)

-- zuerst delete from R_Mietvertrag, dann R_Auto_Preis
set xact_abort on
begin transaction
delete from R_Service where mv_id=@mv_id;
delete from R_Mietvertrag where mv_id=@mv_id;
print('Das Vertrag mit dem mv_id: ' + trim(str(@mv_id)));
print(' und p_id: ' + trim(str(@p_id)) + ' ist st�rniert.'); 
commit;
end
else
print('p_id existiert nicht!');
end;

--*************************************************************
