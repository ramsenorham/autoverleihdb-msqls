USE [AutoVerleihDB]

select * from R_Mietvertrag;
select * from V_Auto_basis_preis;
select * from R_Marke_Modell_Preis;
select dbo.get_basis_preis('VW', 'VW Arteon');
 
exec insertNeuePerson 'D072EGG2177', 'Herr', 'Bahatti', 'Zulqurnain', '01.10.1995', 'Eichenplatz' , '86343', 'Koenigsbrunn';

exec insertNeueMietvertrag 'D072EGG2177', 'BMW','BMW 4er', '20.02.2022','23.02.2022','Navigation,';

select * from V_MietvertragErgebnis where fuehrerschein = 'D072EGG2177';

--update von Mietvertrag: wir �ndern nur das Datum bis.
exec updateMietvertrag '1', '25.02.2022';

select * from V_MietvertragErgebnis where fuehrerschein = 'D072EGG2177';

--Mietvertrag stornieren:
-- L�schen von einem Datensatz aus dem Vertrag: Die Personen, Orte werden nicht gel�scht. 
-- es werden die Service und der Mietvertrag gel�scht.
-- Inkonsistent Check:

exec removeMietvertrag 'D072EGG2177', '20.02.2022';
--Zeige Kunde/Personen, die keinen Vertrag haben.
select * from V_MietvertragErgebnis where fuehrerschein = 'D072EGG2177';

