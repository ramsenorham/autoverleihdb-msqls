-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 01. Mrz 2022 um 00:03
-- Server-Version: 10.4.6-MariaDB
-- PHP-Version: 7.2.21

SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `autoverleihdb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `r_marke_modell_preis`
--

CREATE TABLE `r_marke_modell_preis` (
  `mm_id` int(11) NOT NULL,
  `ma_id` int(11) NOT NULL,
  `mo_id` int(11) NOT NULL,
  `kennzeichen` varchar(9) NOT NULL,
  `mm_preis` double NOT NULL,
  `verfuegbarkeit` varchar(4) NOT NULL
) ;

--
-- Daten für Tabelle `r_marke_modell_preis`
--

INSERT INTO `r_marke_modell_preis` (`mm_id`, `ma_id`, `mo_id`, `kennzeichen`, `mm_preis`, `verfuegbarkeit`) VALUES
(1, 1, 1, 'HH558Z4', 23, 'Ja'),
(2, 1, 2, 'HK98HM8', 24, 'Ja'),
(3, 1, 3, 'HH33HO', 26, 'Ja'),
(4, 1, 4, 'MM678F4', 30, 'Ja'),
(5, 1, 5, 'SE552K', 15, 'Ja'),
(6, 1, 6, 'JK785G', 43, 'Ja'),
(7, 1, 7, 'OP873G', 22, 'Ja'),
(8, 1, 8, 'HQ887H', 44, 'Ja'),
(9, 1, 9, 'D99KJ', 54, 'Ja'),
(10, 4, 10, 'HL9945', 27, 'Ja'),
(11, 4, 11, 'UU6864R', 26, 'Ja'),
(12, 4, 12, 'GH6839H', 20, 'Ja'),
(13, 4, 13, 'WW863G', 33, 'Ja'),
(14, 4, 14, 'AA96732J', 28, 'Ja'),
(15, 4, 15, 'JJD775t', 23, 'Ja'),
(16, 4, 16, 'OI5534H', 24, 'Ja'),
(17, 4, 17, 'UZ8864J', 26, 'Ja'),
(18, 4, 18, 'WE538K', 30, 'Ja');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `r_mietvertrag`
--

CREATE TABLE `r_mietvertrag` (
  `mv_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL,
  `mm_id` int(11) NOT NULL,
  `gesamt_preis` double DEFAULT NULL,
  `vertrag_datum` date NOT NULL,
  `von` date NOT NULL,
  `bis` date NOT NULL
) ;

--
-- Daten für Tabelle `r_mietvertrag`
--

INSERT INTO `r_mietvertrag` (`mv_id`, `p_id`, `mm_id`, `gesamt_preis`, `vertrag_datum`, `von`, `bis`) VALUES
(1, 2, 4, 75, '2022-02-28', '2022-03-01', '2022-03-03');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `r_personadresse`
--

CREATE TABLE `r_personadresse` (
  `p_id` int(11) NOT NULL,
  `o_id` int(11) NOT NULL,
  `strasseNr` varchar(50) NOT NULL,
  `aktuelle_Adresse` char(1) NOT NULL
) ;

--
-- Daten für Tabelle `r_personadresse`
--

INSERT INTO `r_personadresse` (`p_id`, `o_id`, `strasseNr`, `aktuelle_Adresse`) VALUES
(1, 2, 'FriedenStr. 5', '1'),
(2, 4, 'WilhelmStr. 66', '1'),
(3, 1, 'WarStr. 12', '1');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `r_service`
--

CREATE TABLE `r_service` (
  `mv_id` int(11) NOT NULL,
  `zs_id` int(11) NOT NULL
) ;

--
-- Daten für Tabelle `r_service`
--

INSERT INTO `r_service` (`mv_id`, `zs_id`) VALUES
(1, 2);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_auto_marke`
--

CREATE TABLE `t_auto_marke` (
  `ma_id` int(11) NOT NULL,
  `marke` varchar(30) NOT NULL
) ;

--
-- Daten für Tabelle `t_auto_marke`
--

INSERT INTO `t_auto_marke` (`ma_id`, `marke`) VALUES
(1, 'BMW'),
(2, 'Benz'),
(3, 'Ford'),
(4, 'VW'),
(5, 'Opel'),
(6, 'Audi'),
(7, 'Toyota'),
(8, 'Skoda'),
(9, 'Volvo'),
(10, 'Porsche');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_auto_modell`
--

CREATE TABLE `t_auto_modell` (
  `mo_id` int(11) NOT NULL,
  `modell` varchar(30) NOT NULL
) ;

--
-- Daten für Tabelle `t_auto_modell`
--

INSERT INTO `t_auto_modell` (`mo_id`, `modell`) VALUES
(1, 'BMW 2er Gran Tourer'),
(2, 'BMW 1er'),
(3, 'BMW 2er'),
(4, 'BMW 4er'),
(5, 'BMW i3'),
(6, 'BMW X3'),
(7, 'BMW X5'),
(8, 'BMW X5'),
(9, 'BMW X6'),
(10, 'VW Amarok'),
(11, 'VW Arteon'),
(12, 'VW Beetle'),
(13, 'VW Caddy'),
(14, 'VW Crafter'),
(15, 'VW Golf'),
(16, 'VW Polo'),
(17, 'VW T-Cross'),
(18, 'VW CrossPolo');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_ort`
--

CREATE TABLE `t_ort` (
  `o_id` int(11) NOT NULL,
  `plz` char(5) NOT NULL,
  `ort` varchar(30) NOT NULL
) ;

--
-- Daten für Tabelle `t_ort`
--

INSERT INTO `t_ort` (`o_id`, `plz`, `ort`) VALUES
(1, '30161', 'Hannover'),
(2, '52062', 'Kreisfreie Stadt'),
(3, '78267', 'Freiburg'),
(4, '54298', 'Trier-Saarburg'),
(5, '93326', 'Kelheim'),
(6, '97355', 'Kitzingen'),
(7, '30167', 'Hannover'),
(8, '30159', 'Hannover');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_person`
--

CREATE TABLE `t_person` (
  `p_id` int(11) NOT NULL,
  `fuehrerschein` char(11) NOT NULL,
  `titel` char(10) DEFAULT NULL,
  `vorname` varchar(50) DEFAULT NULL,
  `nachname` varchar(50) NOT NULL,
  `geboren` date DEFAULT NULL,
  `eintritt` date NOT NULL
) ;

--
-- Daten für Tabelle `t_person`
--

INSERT INTO `t_person` (`p_id`, `fuehrerschein`, `titel`, `vorname`, `nachname`, `geboren`, `eintritt`) VALUES
(1, 'B072RRE2152', 'Herr', 'Martin', 'Merkel', '1999-08-20', '2017-04-22'),
(2, 'B074ZZE2142', 'Frau', 'Sabine', 'Smidt', '1975-08-03', '2018-06-16'),
(3, 'B072MME2144', 'Frau', 'Elke', 'Pilmaier', '1968-02-15', '2018-07-16');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `t_zusatzservice`
--

CREATE TABLE `t_zusatzservice` (
  `zs_id` int(11) NOT NULL,
  `bezeichnung` varchar(50) NOT NULL,
  `zs_preis` double NOT NULL
) ;

--
-- Daten für Tabelle `t_zusatzservice`
--

INSERT INTO `t_zusatzservice` (`zs_id`, `bezeichnung`, `zs_preis`) VALUES
(1, 'Navigation', 5),
(2, 'Vollkasko ohne Selbstbeteiligung', 15),
(3, 'Schneeketten', 25),
(4, 'Reifenversicherung', 60);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `r_marke_modell_preis`
--
ALTER TABLE `r_marke_modell_preis`
  ADD PRIMARY KEY (`mm_id`,`ma_id`,`mo_id`),
  ADD KEY `ma_id` (`ma_id`),
  ADD KEY `mo_id` (`mo_id`);

--
-- Indizes für die Tabelle `r_mietvertrag`
--
ALTER TABLE `r_mietvertrag`
  ADD PRIMARY KEY (`mv_id`,`p_id`,`mm_id`),
  ADD KEY `p_id` (`p_id`),
  ADD KEY `mm_id` (`mm_id`);

--
-- Indizes für die Tabelle `r_personadresse`
--
ALTER TABLE `r_personadresse`
  ADD PRIMARY KEY (`p_id`,`o_id`),
  ADD KEY `o_id` (`o_id`);

--
-- Indizes für die Tabelle `r_service`
--
ALTER TABLE `r_service`
  ADD PRIMARY KEY (`mv_id`,`zs_id`),
  ADD KEY `zs_id` (`zs_id`);

--
-- Indizes für die Tabelle `t_auto_marke`
--
ALTER TABLE `t_auto_marke`
  ADD PRIMARY KEY (`ma_id`);

--
-- Indizes für die Tabelle `t_auto_modell`
--
ALTER TABLE `t_auto_modell`
  ADD PRIMARY KEY (`mo_id`);

--
-- Indizes für die Tabelle `t_ort`
--
ALTER TABLE `t_ort`
  ADD PRIMARY KEY (`o_id`);

--
-- Indizes für die Tabelle `t_person`
--
ALTER TABLE `t_person`
  ADD PRIMARY KEY (`p_id`);

--
-- Indizes für die Tabelle `t_zusatzservice`
--
ALTER TABLE `t_zusatzservice`
  ADD PRIMARY KEY (`zs_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `r_marke_modell_preis`
--
ALTER TABLE `r_marke_modell_preis`
  MODIFY `mm_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `r_mietvertrag`
--
ALTER TABLE `r_mietvertrag`
  MODIFY `mv_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `t_auto_marke`
--
ALTER TABLE `t_auto_marke`
  MODIFY `ma_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `t_auto_modell`
--
ALTER TABLE `t_auto_modell`
  MODIFY `mo_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `t_ort`
--
ALTER TABLE `t_ort`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `t_person`
--
ALTER TABLE `t_person`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `t_zusatzservice`
--
ALTER TABLE `t_zusatzservice`
  MODIFY `zs_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `r_marke_modell_preis`
--
ALTER TABLE `r_marke_modell_preis`
  ADD CONSTRAINT `r_marke_modell_preis_ibfk_1` FOREIGN KEY (`ma_id`) REFERENCES `t_auto_marke` (`ma_id`),
  ADD CONSTRAINT `r_marke_modell_preis_ibfk_2` FOREIGN KEY (`mo_id`) REFERENCES `t_auto_modell` (`mo_id`);

--
-- Constraints der Tabelle `r_mietvertrag`
--
ALTER TABLE `r_mietvertrag`
  ADD CONSTRAINT `r_mietvertrag_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `t_person` (`p_id`),
  ADD CONSTRAINT `r_mietvertrag_ibfk_2` FOREIGN KEY (`mm_id`) REFERENCES `r_marke_modell_preis` (`mm_id`);

--
-- Constraints der Tabelle `r_personadresse`
--
ALTER TABLE `r_personadresse`
  ADD CONSTRAINT `r_personadresse_ibfk_1` FOREIGN KEY (`p_id`) REFERENCES `t_person` (`p_id`),
  ADD CONSTRAINT `r_personadresse_ibfk_2` FOREIGN KEY (`o_id`) REFERENCES `t_ort` (`o_id`);

--
-- Constraints der Tabelle `r_service`
--
ALTER TABLE `r_service`
  ADD CONSTRAINT `r_service_ibfk_1` FOREIGN KEY (`zs_id`) REFERENCES `t_zusatzservice` (`zs_id`),
  ADD CONSTRAINT `r_service_ibfk_2` FOREIGN KEY (`mv_id`) REFERENCES `r_mietvertrag` (`mv_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
